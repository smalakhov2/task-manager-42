<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<jsp:include page="../../include/_header.jsp"/>

<h2>REGISTRATION</h2>

<form:form action="/registration" method="POST" modelAttribute="newUser">
    <form:input type="hidden" path="id" />

    <p>
        <div style="margin-bottom: 5px;">LOGIN:</div>
        <div><form:input type="text" path="login" /></div>
    </p>

    <p>
        <div style="margin-bottom: 5px;">PASSWORD:</div>
        <div><form:input type="password" path="passwordHash" /></div>
    </p>

    <button type="submit">REGISTRATION</button>

</form:form>




<jsp:include page="../../include/_footer.jsp"/>

