package ru.malakhov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.malakhov.tm.dto.CustomUser;
import ru.malakhov.tm.dto.ProjectDTO;
import ru.malakhov.tm.dto.TaskDTO;
import ru.malakhov.tm.api.service.IProjectService;
import ru.malakhov.tm.api.service.ITaskService;
import ru.malakhov.tm.enumeration.Status;

import java.util.List;

@Controller
public class TaskController {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IProjectService projectService;

    @ModelAttribute("statuses")
    public Status[] getStatus() {
        return Status.values();
    }

    @ModelAttribute("projects")
    private List<ProjectDTO> getProjects(@AuthenticationPrincipal CustomUser user) {
        return projectService.findAllDTOByUserId(user.getUserId());
    }

    @GetMapping("/task/create")
    public String create(@AuthenticationPrincipal CustomUser user) {
        taskService.create(user.getUserId(), "New task");
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    public String delete(@AuthenticationPrincipal CustomUser user, @PathVariable("id") String id) {
        taskService.removeOneById(user.getUserId(), id);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(@AuthenticationPrincipal CustomUser user, @PathVariable("id") String id) {
        final @NotNull TaskDTO taskDTO = taskService.findOneByIdDTO(user.getUserId(), id);
        return new ModelAndView("task/task-edit", "task", taskDTO);
    }

    @PostMapping("/task/edit/{id}")
    public String edit(@AuthenticationPrincipal CustomUser user, @ModelAttribute("task")TaskDTO taskDTO, BindingResult result) {
        if (taskDTO.getProjectId().isEmpty()) taskDTO.setProjectId(null);
        taskService.updateDTO(user.getUserId(), taskDTO);
        return "redirect:/tasks";
    }

}