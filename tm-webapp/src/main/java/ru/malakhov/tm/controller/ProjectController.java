package ru.malakhov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.malakhov.tm.dto.CustomUser;
import ru.malakhov.tm.dto.ProjectDTO;
import ru.malakhov.tm.api.service.IProjectService;
import ru.malakhov.tm.enumeration.Status;

@Controller
public class ProjectController {

    @Autowired
    private IProjectService projectService;

    @ModelAttribute("statuses")
    public Status[] getStatus() {
        return Status.values();
    }

    @GetMapping("/project/create")
    public String create(@AuthenticationPrincipal CustomUser user) {
        projectService.create(user.getUserId(), "New project");
        return "redirect:/projects";
    }

    @GetMapping("/project/delete/{id}")
    public String delete(@AuthenticationPrincipal CustomUser user, @PathVariable("id") String id) {
        projectService.removeOneById(user.getUserId(), id);
        return "redirect:/projects";
    }

    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(@AuthenticationPrincipal CustomUser user, @PathVariable("id") String id) {
        final @NotNull ProjectDTO project = projectService.findOneByIdDTO(user.getUserId(), id);
        return new ModelAndView("project/project-edit", "project", project);
    }

    @PostMapping("/project/edit/{id}")
    public String edit(@AuthenticationPrincipal CustomUser user, @ModelAttribute("project") ProjectDTO project, BindingResult result) {
        projectService.updateDTO(user.getUserId(), project);
        return "redirect:/projects";
    }
}