package ru.malakhov.tm.repository.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.malakhov.tm.entity.AbstractEntity;

@Repository
public interface IRepository<E extends AbstractEntity> extends JpaRepository<E, String> {
}