package ru.malakhov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.malakhov.tm.dto.ProjectDTO;

import java.util.List;

@Repository
public interface IProjectRepositoryDTO extends IRepositoryDTO<ProjectDTO> {

    @Nullable
    List<ProjectDTO> findAllByUserId(final @NotNull String userId);

    @NotNull
    ProjectDTO findOneByUserIdAndId(final @NotNull String userId, final @NotNull String id);

    boolean existsByUserIdAndId(String userId, String id);

}