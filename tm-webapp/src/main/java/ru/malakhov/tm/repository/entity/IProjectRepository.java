package ru.malakhov.tm.repository.entity;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.malakhov.tm.entity.Project;

@Repository
public interface IProjectRepository extends IRepository<Project> {

    void deleteAllByUserId(final @NotNull String userId);

    void deleteOneByUserIdAndId(final @NotNull String userId, final @NotNull String id);

}