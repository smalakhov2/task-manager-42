package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.malakhov.tm.dto.UserDTO;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.exception.empty.EmptyIdException;
import ru.malakhov.tm.exception.empty.EmptyLoginException;
import ru.malakhov.tm.exception.empty.EmptyPasswordException;
import ru.malakhov.tm.exception.empty.EmptyUserIdException;
import ru.malakhov.tm.repository.dto.IUserRepositoryDTO;
import ru.malakhov.tm.repository.entity.IUserRepository;
import ru.malakhov.tm.api.service.IUserService;
import ru.malakhov.tm.exception.user.AccessDeniedException;
import ru.malakhov.tm.exception.user.InvalidLoginException;
import ru.malakhov.tm.exception.user.UserNotFoundException;

import java.util.List;

@Service
public final class UserService extends AbstractService<User> implements IUserService {

    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private IUserRepositoryDTO userRepositoryDTO;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void create(final @Nullable String login, final @Nullable String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final @Nullable String passwordHash = passwordEncoder.encode(password);
        if (passwordHash == null) throw new AccessDeniedException();
        final @NotNull User user = new User(login, passwordHash);
        if (userRepository.findOneByLogin(user.getLogin()) != null) throw new InvalidLoginException();
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void updateDTO(final @Nullable UserDTO user) {
        if (user == null) throw new UserNotFoundException();
        if (user.getLogin() == null || user.getLogin().isEmpty()) throw new EmptyLoginException();
        if (userRepositoryDTO.findOneByLogin(user.getLogin()) == null) throw new InvalidLoginException();
        if (userRepositoryDTO.findOneByLogin(user.getLogin()).getId() != user.getId()) throw new InvalidLoginException();
        userRepositoryDTO.save(user);
    }

    @Override
    @Transactional
    public void deleteAll() {
        userRepository.deleteAll();
    }

    @Override
    public @NotNull User findById(final @Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final @Nullable User user = userRepository.findOneById(id);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @Override
    public @Nullable UserDTO findByLoginDTO(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepositoryDTO.findOneByLogin(login);
    }

    @Override
    public @NotNull List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public @NotNull UserDTO profile(final @Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyUserIdException();
        return userRepositoryDTO.findOneById(id);
    }

}