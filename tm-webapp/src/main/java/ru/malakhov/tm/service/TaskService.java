package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.malakhov.tm.dto.TaskDTO;
import ru.malakhov.tm.entity.Task;
import ru.malakhov.tm.exception.empty.EmptyIdException;
import ru.malakhov.tm.exception.empty.EmptyNameException;
import ru.malakhov.tm.exception.empty.EmptyUserIdException;
import ru.malakhov.tm.repository.dto.ITaskRepositoryDTO;
import ru.malakhov.tm.repository.entity.ITaskRepository;
import ru.malakhov.tm.api.service.IProjectService;
import ru.malakhov.tm.api.service.ITaskService;
import ru.malakhov.tm.api.service.IUserService;
import ru.malakhov.tm.exception.task.TaskNotFoundException;

import java.util.List;

@Service
public final class TaskService extends AbstractService<Task> implements ITaskService {

    @Autowired
    private IProjectService projectService;

    @Autowired
    private IUserService userService;

    @Autowired
    private ITaskRepository taskRepository;

    @Autowired
    private ITaskRepositoryDTO taskRepositoryDTO;


    @Override
    public void create(final @Nullable String userId,final @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final @NotNull Task task = new Task();
        task.setName(name);
        task.setUser(userService.findById(userId));
        taskRepository.save(task);
    }

    @Override
    public void updateDTO(final @Nullable String userId, final @Nullable TaskDTO taskDTO) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (taskDTO == null) throw new TaskNotFoundException();
        taskDTO.setUserId(userId);
        taskRepositoryDTO.save(taskDTO);
    }

    @Override
    public @NotNull List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public @NotNull List<Task> findAllByUserId(final @Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return taskRepository.findAllByUserId(userId);
    }

    @Override
    public void deleteAll() {
        taskRepository.deleteAll();
    }

    @Override
    public void deleteAllByUserId(final @Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        taskRepository.deleteAllByUserId(userId);
    }

    @Override
    public @NotNull List<TaskDTO> findAllDTOByUserId(final @Nullable String userId) {
        return taskRepositoryDTO.findAllByUserId(userId);
    }

    @Override
    public @NotNull TaskDTO findOneByIdDTO(final @Nullable String userId, final @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final @Nullable TaskDTO taskDTO = taskRepositoryDTO.findOneByUserIdAndId(userId, id);
        if (taskDTO == null) throw new TaskNotFoundException();
        return taskDTO;
    }

    @Override
    @Transactional
    public void removeOneById(final @Nullable String userId, final @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        taskRepository.deleteOneByUserIdAndId(userId, id);
    }

    @Override
    public boolean existsByUserIdAndId(final @Nullable String userId, final @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepositoryDTO.existsByUserIdAndId(userId, id);
    }

}