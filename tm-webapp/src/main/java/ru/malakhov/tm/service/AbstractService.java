package ru.malakhov.tm.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.malakhov.tm.repository.entity.IRepository;
import ru.malakhov.tm.api.service.IService;
import ru.malakhov.tm.entity.AbstractEntity;

import java.util.Collection;

@Service
public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    private IRepository repository;

    @Override
    @Transactional
    public void load(final @Nullable Collection<E> e) {
        if (e == null) return;
        repository.saveAll(e);
    }

}