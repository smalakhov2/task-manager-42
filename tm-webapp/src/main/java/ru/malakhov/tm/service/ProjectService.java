package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.malakhov.tm.dto.ProjectDTO;
import ru.malakhov.tm.entity.Project;
import ru.malakhov.tm.exception.empty.EmptyIdException;
import ru.malakhov.tm.exception.empty.EmptyNameException;
import ru.malakhov.tm.exception.empty.EmptyUserIdException;
import ru.malakhov.tm.exception.project.ProjectNotFoundException;
import ru.malakhov.tm.repository.dto.IProjectRepositoryDTO;
import ru.malakhov.tm.repository.entity.IProjectRepository;
import ru.malakhov.tm.api.service.IProjectService;
import ru.malakhov.tm.api.service.IUserService;

import java.util.List;

@Service
public final class ProjectService extends AbstractService<Project> implements IProjectService {

    @Autowired
    private IUserService userService;

    @Autowired
    private IProjectRepository projectRepository;

    @Autowired
    private IProjectRepositoryDTO projectRepositoryDTO;

    @Override
    @Transactional
    public void create(final @Nullable String userId, final @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final @NotNull Project project = new Project();
        project.setName(name);
        project.setUser(userService.findById(userId));
        projectRepository.save(project);
    }

    @Override
    public void updateDTO(@Nullable String userId, @Nullable ProjectDTO project) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) throw new ProjectNotFoundException();
        project.setUserId(userId);
        projectRepositoryDTO.save(project);
    }

    @Override
    public @NotNull List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public @NotNull List<ProjectDTO> findAllDTOByUserId(final @Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return projectRepositoryDTO.findAllByUserId(userId);
    }

    @Override
    @Transactional
    public void deleteAll() {
        projectRepository.deleteAll();
    }

    @Override
    @Transactional
    public void deleteAllByUserId(final @Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        projectRepository.deleteAllByUserId(userId);
    }

    @Override
    public @NotNull ProjectDTO findOneByIdDTO(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepositoryDTO.findOneByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void removeOneById(final @Nullable String userId, final @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        projectRepository.deleteOneByUserIdAndId(userId, id);
    }

    @Override
    public boolean existsByUserIdAndId(final @Nullable String userId, final @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepositoryDTO.existsByUserIdAndId(userId, id);
    }

}