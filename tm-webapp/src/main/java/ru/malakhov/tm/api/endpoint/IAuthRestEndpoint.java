package ru.malakhov.tm.api.endpoint;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("/api/auth")
public interface IAuthRestEndpoint {

    @GetMapping
    boolean login(
            @RequestParam("login") String login,
            @RequestParam("password") String password
    );

    @GetMapping("/logout")
    void logout();

}
