package ru.malakhov.tm.api.service;


public interface IDataService {

    void binaryClear() throws Exception;

    void binaryLoad() throws Exception;

    void binarySave() throws Exception;

    void jsonClearFasterxml() throws Exception;

    void jsonLoadFasterxml() throws Exception;

    void jsonSaveFasterxml() throws Exception;

    void xmlClearFasterxml() throws Exception;

    void xmlLoadFasterxml() throws Exception;

    void xmlSaveFasterxml() throws Exception;

    void jsonClearJaxb() throws Exception;

    void jsonLoadJaxb() throws Exception;

    void jsonSaveJaxb() throws Exception;

    void xmlClearJaxb() throws Exception;

    void xmlLoadJaxb() throws Exception;

    void xmlSaveJaxb() throws Exception;

}