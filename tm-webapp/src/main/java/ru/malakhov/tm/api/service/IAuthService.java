package ru.malakhov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.enumeration.Role;

public interface IAuthService {

    boolean checkRoles(@Nullable String userId, @Nullable Role[] roles);

    void registration(@Nullable String login, @Nullable String password);

}
