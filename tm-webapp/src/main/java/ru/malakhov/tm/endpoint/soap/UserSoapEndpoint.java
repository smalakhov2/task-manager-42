package ru.malakhov.tm.endpoint.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.api.service.IUserService;
import ru.malakhov.tm.dto.UserDTO;
import ru.malakhov.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebService;

@Component
@WebService
public class UserSoapEndpoint {

    @Autowired
    private IUserService userService;

    @WebMethod
    public UserDTO profile() {
        return userService.profile(UserUtil.getUserId());
    }
    
}
