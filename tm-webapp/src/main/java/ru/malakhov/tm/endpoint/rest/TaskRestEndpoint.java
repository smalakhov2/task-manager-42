package ru.malakhov.tm.endpoint.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.malakhov.tm.api.endpoint.ITaskRestEndpoint;
import ru.malakhov.tm.api.service.ITaskService;
import ru.malakhov.tm.dto.TaskDTO;
import ru.malakhov.tm.util.UserUtil;

@RestController
@RequestMapping("/api/task")
public class TaskRestEndpoint implements ITaskRestEndpoint {

    @Autowired
    private ITaskService taskService;

    @Override
    @PostMapping("/create")
    public void create(@RequestBody String name) {
        taskService.create(UserUtil.getUserId(), name);
    }

    @Override
    @PutMapping
    public void update(@RequestBody TaskDTO task) {
        taskService.updateDTO(UserUtil.getUserId(), task);
    }

    @Override
    @GetMapping("/${id}")
    public TaskDTO findOneByIdDTO(@PathVariable("id") String id) {
        return taskService.findOneByIdDTO(UserUtil.getUserId(), id);
    }

    @Override
    @GetMapping("/exist/${id}")
    public boolean existsById(@PathVariable("id") String id) {
        return taskService.existsByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @DeleteMapping("/${id}")
    public void deleteOneById(@PathVariable("id") String id) {
        taskService.removeOneById(UserUtil.getUserId(), id);
    }

}
