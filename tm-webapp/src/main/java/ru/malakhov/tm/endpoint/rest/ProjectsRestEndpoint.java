package ru.malakhov.tm.endpoint.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.malakhov.tm.api.endpoint.IProjectsRestEndpoint;
import ru.malakhov.tm.api.service.IProjectService;
import ru.malakhov.tm.dto.ProjectDTO;
import ru.malakhov.tm.util.UserUtil;

import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectsRestEndpoint implements IProjectsRestEndpoint {

    @Autowired
    private IProjectService projectService;

    @Override
    @GetMapping
    public List<ProjectDTO> getListDTO() {
        return projectService.findAllDTOByUserId(UserUtil.getUserId());
    }

    @Override
    @DeleteMapping("/all")
    public void deleteAll() {
        projectService.deleteAllByUserId(UserUtil.getUserId());
    }

}
