package ru.malakhov.tm.endpoint.soap;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.api.service.IAuthService;
import ru.malakhov.tm.api.service.IUserService;
import ru.malakhov.tm.dto.UserDTO;
import ru.malakhov.tm.dto.status.Result;
import ru.malakhov.tm.exception.user.AccessDeniedException;
import ru.malakhov.tm.exception.user.UserNotFoundException;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Component
@WebService
public class AuthSoapEndpoint {

    @Autowired
    private IUserService userService;

    @Autowired
    private IAuthService authService;

    @Resource
    private AuthenticationManager authenticationManager;

    @WebMethod
    public Result login(
            @WebParam(name = "login") final String login,
            @WebParam(name = "password") final String password
    ) {
        final UserDTO userDTO = userService.findByLoginDTO(login);
        if (userDTO == null) throw new UserNotFoundException();
        if (userDTO.isLocked()) throw new AccessDeniedException();
        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(login, password);
        final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return new Result(authentication.isAuthenticated());
    }

    @WebMethod
    public Result logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return new Result();
    }

    @WebMethod
    public void registration(
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password
    ) {
        authService.registration(login, password);
    }

}
