package ru.malakhov.tm.exception.user;

import ru.malakhov.tm.exception.AbstractException;

public final class UserNotFoundException extends AbstractException {

    public UserNotFoundException() {
        super("Error! User not found...");
    }

}