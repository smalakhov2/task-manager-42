package ru.malakhov.tm.listener.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.endpoint.soap.AuthSoapEndpoint;
import ru.malakhov.tm.event.ConsoleEvent;
import ru.malakhov.tm.listener.AbstractListener;
import ru.malakhov.tm.util.TerminalUtil;

@Component
public class LoginListener extends AbstractListener {

    @Autowired
    private AuthSoapEndpoint authSoapEndpoint;

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String command() {
        return "login";
    }

    @Override
    public @NotNull String description() {
        return "Login.";
    }

    @Override
    @EventListener(condition = "@loginListener.command() == #event.command")
    public void handle(final ConsoleEvent event) {
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN:");
        final @Nullable String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final @Nullable String password = TerminalUtil.nextLine();
        authSoapEndpoint.login(login, password);
        sessionService.saveCookieHeaders(authSoapEndpoint);
        System.out.println("[OK]");
    }

}
