package ru.malakhov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.event.ConsoleEvent;
import ru.malakhov.tm.listener.AbstractListener;

import java.util.List;

@Component
public class HelpListener extends AbstractListener {

    @Autowired
    private List<AbstractListener> commands;

    @Override
    public @NotNull String arg() {
        return "-h";
    }

    @Override
    public @NotNull String command() {
        return "help";
    }

    @Override
    public @NotNull String description() {
        return "Display list of terminal commands.";
    }

    @Override
    @EventListener(condition = "@helpListener.command() == #event.command")
    public void handle(final ConsoleEvent event) {
        System.out.println("[HELP]");
        for (final @NotNull AbstractListener command: commands) {
            if (command.arg() == null) continue;
            System.out.println(command.command() + " - " + command.description());
        }
        System.out.println("[OK]");
    }

}
