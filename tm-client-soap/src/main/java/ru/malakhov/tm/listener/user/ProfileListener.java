package ru.malakhov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.endpoint.soap.UserSoapEndpoint;
import ru.malakhov.tm.event.ConsoleEvent;
import ru.malakhov.tm.listener.AbstractListener;

@Component
public class ProfileListener extends AbstractListener {

    @Autowired
    private UserSoapEndpoint userSoapEndpoint;

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String command() {
        return "profile";
    }

    @Override
    public @NotNull String description() {
        return "Profile.";
    }

    @Override
    @EventListener(condition = "@profileListener.command() == #event.command")
    public void handle(final ConsoleEvent event) {
        System.out.println("[PROFILE]");
        sessionService.setListCookieRowRequest(userSoapEndpoint);
        System.out.println(userSoapEndpoint.profile().toString());
        System.out.println("[OK]");
    }

}
