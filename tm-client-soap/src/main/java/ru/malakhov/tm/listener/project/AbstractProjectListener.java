package ru.malakhov.tm.listener.project;

import org.springframework.beans.factory.annotation.Autowired;
import ru.malakhov.tm.endpoint.soap.ProjectSoapEndpoint;
import ru.malakhov.tm.listener.AbstractListener;

public abstract class AbstractProjectListener extends AbstractListener {

    @Autowired
    protected ProjectSoapEndpoint projectEndpoint;

}