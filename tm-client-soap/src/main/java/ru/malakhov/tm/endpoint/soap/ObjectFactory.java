
package ru.malakhov.tm.endpoint.soap;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.malakhov.tm.endpoint.soap package. 
 * &lt;p&gt;An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Profile_QNAME = new QName("http://soap.endpoint.tm.malakhov.ru/", "profile");
    private final static QName _ProfileResponse_QNAME = new QName("http://soap.endpoint.tm.malakhov.ru/", "profileResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.malakhov.tm.endpoint.soap
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Profile }
     * 
     */
    public Profile createProfile() {
        return new Profile();
    }

    /**
     * Create an instance of {@link ProfileResponse }
     * 
     */
    public ProfileResponse createProfileResponse() {
        return new ProfileResponse();
    }

    /**
     * Create an instance of {@link UserDTO }
     * 
     */
    public UserDTO createUserDTO() {
        return new UserDTO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Profile }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Profile }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.malakhov.ru/", name = "profile")
    public JAXBElement<Profile> createProfile(Profile value) {
        return new JAXBElement<Profile>(_Profile_QNAME, Profile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProfileResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ProfileResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.malakhov.ru/", name = "profileResponse")
    public JAXBElement<ProfileResponse> createProfileResponse(ProfileResponse value) {
        return new JAXBElement<ProfileResponse>(_ProfileResponse_QNAME, ProfileResponse.class, null, value);
    }

}
