package ru.malakhov.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.event.ConsoleEvent;
import ru.malakhov.tm.util.TerminalUtil;

import java.lang.Exception;

@Component
public final class Bootstrap {

    @Autowired
    private @NotNull ApplicationEventPublisher publisher;

    private static void displayHello() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
    }

    private void process() {
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                logError(e);
            }
        }
    }

    private boolean parseArgs(final @Nullable String[] args) {
        if (args == null || args.length < 1) return false;
        final @NotNull String arg = args[0];
        try {
            parseCommand(arg);
        } catch (Exception e) {
            logError(e);
        }
        return true;
    }

    private static void logError(@NotNull Exception e){
        System.err.println(e.getMessage());
        System.err.println("[FAIL]");
    }

    private void parseCommand(final @Nullable String cmd) {
        if (cmd == null || cmd.isEmpty()) return;
        publisher.publishEvent(new ConsoleEvent(cmd));
    }

    public void run(final String[] args) {
        displayHello();
        if (parseArgs(args)) System.exit(0);
        process();
    }

}