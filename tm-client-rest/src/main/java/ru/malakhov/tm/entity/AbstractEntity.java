package ru.malakhov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class AbstractEntity {

    @NotNull
    private String id = UUID.randomUUID().toString();

    public @NotNull String getId() {
        return id;
    }

}