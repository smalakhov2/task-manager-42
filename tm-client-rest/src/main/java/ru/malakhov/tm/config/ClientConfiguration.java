package ru.malakhov.tm.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("ru.malakhov.tm")
public class ClientConfiguration {
}