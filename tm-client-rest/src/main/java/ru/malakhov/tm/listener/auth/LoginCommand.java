package ru.malakhov.tm.listener.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.api.endpoint.IAuthRestEndpoint;
import ru.malakhov.tm.event.ConsoleEvent;
import ru.malakhov.tm.listener.AbstractListener;
import ru.malakhov.tm.util.TerminalUtil;

@Component
public class LoginCommand extends AbstractListener {

    @Autowired
    private IAuthRestEndpoint authRestEndpoint;

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String command() {
        return "login";
    }

    @Override
    public @NotNull String description() {
        return "Login.";
    }

    @Async
    @Override
    @EventListener(condition = "@loginListener.command() == #event.command")
    public void handle(final ConsoleEvent event) {
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN:");
        final @Nullable String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final @Nullable String password = TerminalUtil.nextLine();
        authRestEndpoint.login(login, password);
        System.out.println("[OK]");
    }

}