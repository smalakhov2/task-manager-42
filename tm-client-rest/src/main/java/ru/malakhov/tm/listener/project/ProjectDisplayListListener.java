package ru.malakhov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.api.endpoint.IProjectsRestEndpoint;
import ru.malakhov.tm.constant.SystemConstant;
import ru.malakhov.tm.event.ConsoleEvent;
import ru.malakhov.tm.listener.AbstractListener;

@Component
public class ProjectDisplayListListener extends AbstractListener {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String command() {
        return "project-list";
    }

    @Override
    public @NotNull String description() {
        return "Display task projects.";
    }

    @Async
    @Override
    @EventListener(condition = "@projectDisplayListListener.command() == #event.command")
    public void handle(final ConsoleEvent event) {
        System.out.println("[PROJECT LIST]");
        System.out.println(IProjectsRestEndpoint.projectsClient(SystemConstant.host).getListDTO());
        System.out.println("[OK]");
    }

}