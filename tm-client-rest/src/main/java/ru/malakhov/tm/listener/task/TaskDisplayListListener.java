package ru.malakhov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.api.endpoint.ITasksRestEndpoint;
import ru.malakhov.tm.constant.SystemConstant;
import ru.malakhov.tm.event.ConsoleEvent;
import ru.malakhov.tm.listener.AbstractListener;

@Component
public class TaskDisplayListListener extends AbstractListener {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String command() {
        return "task-list";
    }

    @Override
    public @NotNull String description() {
        return "Display task list.";
    }

    @Override
    @EventListener(condition = "@taskDisplayListListener.command() == #event.command")
    public void handle(final ConsoleEvent event) {
        System.out.println("[TASK LIST]");
        System.out.println(ITasksRestEndpoint.tasksClient(SystemConstant.host).getListDTO());
        System.out.println("[OK]");
    }

}