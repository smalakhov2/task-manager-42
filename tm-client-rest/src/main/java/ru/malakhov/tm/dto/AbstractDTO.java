package ru.malakhov.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
public abstract class AbstractDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    private String id = UUID.randomUUID().toString();

    public @NotNull String getId() {
        return id;
    }

    public void setId(@NotNull String id) {
        this.id = id;
    }

}