package ru.malakhov.tm.api.endpoint;

import feign.Feign;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.malakhov.tm.dto.TaskDTO;

@RequestMapping("/api/task")
public interface ITaskRestEndpoint {

    static ITaskRestEndpoint taskClient(final String baseUrl) {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(ITaskRestEndpoint.class, baseUrl);
    }

    @PostMapping
    void create(@RequestBody String name);

    @PutMapping
    void update(@RequestBody TaskDTO task);

    @GetMapping("/${id}")
    TaskDTO findOneByIdDTO(@PathVariable("id") String id);

    @GetMapping("/exist/${id}")
    boolean existsById(@PathVariable("id") String id);

    @DeleteMapping("/${id}")
    void deleteOneById(@PathVariable("id") String id);

}